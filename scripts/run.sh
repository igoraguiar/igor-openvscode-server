#!/bin/bash

source .env

! [ -d var ] && mkdir -v var

docker run -v "$(pwd)/var:/home/workspace:cached" --name ovscode  --rm -it igor-openvscode-server:$OVSCODE_VERSION