#!/bin/bash

source .env

TAG=igor-openvscode-server:${OVSCODE_VERSION}-${RELEASE_VERSION}
echo "Building TAG=${TAG}, OVSCODE_VERSION=${OVSCODE_VERSION}, ASDF_VERSION=${ASDF_VERSION}"
docker build --pull --rm -f "Dockerfile" \
    --build-arg OVSCODE_VERSION=$OVSCODE_VERSION \
    --build-arg ASDF_VERSION=$ASDF_VERSION \
    -t $TAG "."