USER=$(whoami)

if [ "$USER" = "root" ]
then
    exit 0
fi

DIR=$HOME

if ! [ -d "$DIR/.asdf" ]
then
    git clone https://github.com/asdf-vm/asdf.git $DIR/.asdf --branch ${ASDF_VERSION} 
fi

if ! [ -e "$DIR/.bashrc" ]
then
    cp -va /docker-files/bashrc/.bashrc* "$DIR/"
fi