ARG OVSCODE_VERSION=latest

FROM gitpod/openvscode-server:${OVSCODE_VERSION}

ARG OVSCODE_VERSION=latest
ARG ASDF_VERSION=v0.9.0
ENV ASDF_VERSION=${ASDF_VERSION}
ENV OVSCODE_VERSION=${OVSCODE_VERSION}

RUN sudo apt update \
    && sudo apt install -y --no-install-recommends curl tar gpg zip bzip2 wget bash-completion \
    && sudo apt-get clean && sudo rm -rf /var/cache/apt/* && sudo rm -rf /var/lib/apt/lists/* && sudo rm -rf /tmp/*

# RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch ${ASDF_VERSION}

COPY docker-files /docker-files
COPY docker-files/profile.sh /etc/profile.d/check-bashrc.sh